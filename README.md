# freebsd-raspberry-pi



freebsd desktop for the PI   (example: rpi3b)

![](media/freebsd-raspberry-pi.png)


## Getting started

Image: 
https://gitlab.com/openbsd98324/freebsd-raspberry-pi/-/raw/main/image/FreeBSD-13.0-CURRENT-arm64-aarch64-RPI3-20181213-r342020.img.xz

xzcat  ... the image onto your  /dev/sdX  SD/MMC.


## Installation


```` 
pkg install xorg 

pkg install icewm 

adduser 

```` 

## Usage

Create .xinitrc and then: 

```` 
startx 

```` 


